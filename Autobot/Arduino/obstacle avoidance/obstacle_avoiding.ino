
#include <Servo.h>          //Servo motor library. This is standard library


//our L298N control pins
const int LeftMotorForward = 6;
const int LeftMotorBackward = 7;
const int RightMotorForward = 5;
const int RightMotorBackward = 4;

//sensor pins
const unsigned int TRIG_PIN = A0;
const unsigned int ECHO_PIN = A1;
const unsigned int BAUD_RATE = 9600;


#define maximum_distance 200
boolean goesForward = false;
int distance = 100;

Servo servo_motor; //our servo name


void setup() {

  pinMode(RightMotorForward, OUTPUT);
  pinMode(LeftMotorForward, OUTPUT);
  pinMode(LeftMotorBackward, OUTPUT);
  pinMode(RightMotorBackward, OUTPUT);

  pinMode(TRIG_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);
  Serial.begin(BAUD_RATE);

  servo_motor.attach(10); //our servo pin

  servo_motor.write(120);
  delay(2000);
  distance = readDistance();
  delay(100);
  distance = readDistance();
  delay(100);
  distance = readDistance();
  delay(100);
  distance = readDistance();
  delay(100);
}

void loop() {

  int distanceRight = 0;
  int distanceLeft = 0;
  delay(50);

  if (distance <= 25) {
    moveStop();
    delay(300);
    moveBackward();
    delay(400);
    moveStop();
    delay(300);
   // ScanWidthObstacle();
    distanceRight = lookRight();
    Serial.println("kcach phai");
    Serial.println(distanceRight);
    delay(500);
    distanceLeft = lookLeft();
    Serial.println("kcach trai");
    Serial.println(distanceLeft);
    delay(500);

    if (distanceRight >= distanceLeft) {
      if(distanceRight >=20){
        turnRight();
        moveStop();
        Serial.println("re phai");
      }
      else {
        moveBackward();
        delay(400);
        moveStop();
        delay(300);
        Serial.println("lui lai");
      }
    }
    else {
      if(distanceLeft >=20){
          turnLeft();
          moveStop();
          Serial.println("re trai");
      }
      else {
        moveBackward();
        delay(400);
        moveStop();
        delay(300);
        Serial.println("lui lai");
      }
    }
  }
  else {
    moveForward();
    
  }
  distance = readDistance();
  Serial.println(distance);
}

int lookRight() {
  servo_motor.write(30);
  delay(1000);
  int distance = readDistance();
  delay(100);
  servo_motor.write(120);
  return distance;
  delay(100);
}

int lookLeft() {
  servo_motor.write(210);
  delay(1000);
  int distance = readDistance();
  delay(100);
  servo_motor.write(120);
  return distance;
  delay(100);
}
/*void ScanWidthObstacle(){
  int degree =80;
  while (degree<=160){
        servo_motor.write(degree);
        delay(100);
        int distance = readDistance();
        delay(100);
        Serial.println("khoang cach o goc ");
        Serial.print("goc  ");
        Serial.print(degree-120);
        Serial.print("  khoang cach  ");  
        Serial.println(distance);
        degree=degree+10;
  }

}*/

int readDistance() {
  digitalWrite(TRIG_PIN, LOW);
  delayMicroseconds(2);
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);
  const unsigned long duration = pulseIn(ECHO_PIN, HIGH);
  int distance = duration / 58;
  return distance;
  // return duration;
}

void moveStop() {

  digitalWrite(RightMotorForward, LOW);
  digitalWrite(LeftMotorForward, LOW);
  digitalWrite(RightMotorBackward, LOW);
  digitalWrite(LeftMotorBackward, LOW);
}

void moveForward() {

  if (!goesForward) {

    goesForward = true;

    digitalWrite(LeftMotorForward, HIGH);
    digitalWrite(RightMotorForward, HIGH);

    digitalWrite(LeftMotorBackward, LOW);
    digitalWrite(RightMotorBackward, LOW);
  }
}

void moveBackward() {

  goesForward = false;

  digitalWrite(LeftMotorBackward, HIGH);
  digitalWrite(RightMotorBackward, HIGH);

  digitalWrite(LeftMotorForward, LOW);
  digitalWrite(RightMotorForward, LOW);

}

void turnRight() {

  digitalWrite(LeftMotorForward, HIGH);
  digitalWrite(RightMotorBackward, HIGH);

  digitalWrite(LeftMotorBackward, LOW);
  digitalWrite(RightMotorForward, LOW);

  delay(400);

  digitalWrite(LeftMotorForward, HIGH);
  digitalWrite(RightMotorForward, HIGH);

  digitalWrite(LeftMotorBackward, LOW);
  digitalWrite(RightMotorBackward, LOW);



}

void turnLeft() {

  digitalWrite(LeftMotorBackward, HIGH);
  digitalWrite(RightMotorForward, HIGH);

  digitalWrite(LeftMotorForward, LOW);
  digitalWrite(RightMotorBackward, LOW);

  delay(400);

  digitalWrite(LeftMotorForward, HIGH);
  digitalWrite(RightMotorForward, HIGH);

  digitalWrite(LeftMotorBackward, LOW);
  digitalWrite(RightMotorBackward, LOW);
}

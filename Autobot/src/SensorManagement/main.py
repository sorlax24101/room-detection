from RangeFinder import Rangefinder
from time import sleep

import threading

#finder1 = Rangefinder(31, 33);
finder2 = Rangefinder(19, 26);

#finder1.setup();
finder2.setup();

class soundThread(threading.Thread):
    def __init__(self, threadID, name, counter, trigger, echo):
        threading.Thread.__init__(self);
        self.name = name;
        self.counter = counter;
        self.finder = Rangefinder(trigger, echo);
        self.finder.setup();
        pass

    def run(self):
        try:
            while True:
                self.finder.rangeFinder();
                sleep(1);

        except KeyboardInterrupt:
            print("thread % stopped" % self.name);
            self.finder.stop();
        pass

thread1 = soundThread(1, "Range finder 1", 1, 19, 26);
thread2 = soundThread(2, "Range finder 2", 2, 23, 24);

thread1.start();
thread2.start();

try:
    while True:
        distance1 = thread1.finder.distance;
        distance2 = thread2.finder.distance;
        print("lower ultrasound: ", distance1);
        print("Upper ultrasound: ", distance2);
        sleep(1);
        if distance1 < 30 or distance2 < 30:
            if distance1 != distance2:
                print("Found rubbish");
            else:
                print("Road blocked!!!");

except KeyboardInterrupt:
    print("Measured distance stopped by user")

thread1.join();
thread2.join();

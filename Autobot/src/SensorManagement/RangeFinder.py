import RPi.GPIO as GPIO
import time

class Rangefinder():
    def __init__(self, trigger, echo, distance = 0):
        self.trigger = trigger;
        self.echo = echo;
        self.distance = distance;
        pass

    def setup(self):
        # GPIO mode(BOARD / BCM)
        GPIO.setmode(GPIO.BCM);

        # Set GPIO direction (IN / OUT)
        GPIO.setup(self.trigger, GPIO.OUT);
        GPIO.setup(self.echo, GPIO.IN);

    def stop(self):
        GPIO.cleanup();

    def rangeFinder(self):
        # Set trigger to high
        GPIO.output(self.trigger, True);
    
        # Set trigger after .01s to LOW
        time.sleep(0.00001);
        GPIO.output(self.trigger, False)

        startTime = time.time();
        stopTime = time.time();

        # Save start time
        while GPIO.input(self.echo) == 0:
            startTime = time.time();

        # Save time of arrival
        while GPIO.input(self.echo) == 1:
            stopTime = time.time();

        # Time different between start and arrival
        timeElapsed = stopTime - startTime;

        # distance formula: distance = time * supersonic speed (34300 cm/s)
        # Since it will travel 2 times (current -> destination, destination -> current) => divided by 2
        self.distance = (timeElapsed * 34300) // 2;

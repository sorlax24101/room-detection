cmake_minimum_required(VERSION 3.7)
project(cleanbot)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_C_COMPILER avr-gcc)
set(CMAKE_C_FLAGS "-std=gnu99 -lwiringPi -lwiringPiDev")

include_directories(
	/usr/lib/avr/include
	/usr/lib/include
	)

add_executable( cleanbot
	"MapManagement/mapReader.cpp"
	"MapManagement/mapReader.h"
	"SensorManagement/Rangefinder.cpp"
	)

import RPi.GPIO as GPIO
import time

trigger_1 = 27;
trigger_2 = 17;
echo_1 = 4;
echo_2 = 18;

def setup():
    global echo_1;
    global echo_2;
    global trigger_1;
    global trigger_2;

    GPIO.setmode(GPIO.BCM);

    GPIO.setup(trigger_1, GPIO.OUT);
    GPIO.setup(trigger_2, GPIO.OUT);
    GPIO.setup(echo_1, GPIO.IN);
    GPIO.setup(echo_2, GPIO.IN);
    pass

def us1_distance():
    global trigger_1;
    global echo_1;

    GPIO.output(trigger_1, True);
    time.sleep(0.00001);
    GPIO.output(trigger_1, False);

    starttime = time.time();
    stoptime = time.time();

    while GPIO.input(echo_1) == 0:
        starttime = time.time();
    while GPIO.input(echo_1) == 1:
        stoptime = time.time();

    elapsetime = stoptime - starttime;
    return ((elapsetime * 34300) / 2);

def us2_distance():
    global trigger_2;
    global echo_2;

    GPIO.output(trigger_2, True);

if __name__ == '__main__':
    setup();

cmake_minimum_required(VERSION 3.7)
project(US-Project)

set(CMAKE_CXX_STANDARD 14)
#SET(CMAKE_SYSTEM_NAME GENERIC)
SET(CMAKE_C_COMPILER avr-gcc)
SET(CMAKE_C_FLAGS "-std=gnu99 -lwiringPi -lwiringPiDev")

include_directories(
	/usr/lib/avr/include
	/usr/lib/include
	)

add_executable( US-Project
	"classification.cpp"
	"distance-testing.cpp"
	"randomForest.cpp"
	"header/classification.h"
	"header/randomForest.h"
	)

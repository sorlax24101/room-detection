import RPi.GPIO as GPIO
import time

# GPIO mode(BOARD / BCM)
GPIO.setmode(GPIO.BCM);

# Set PINs
TRIGGER = 19;
ECHO = 26;

# Set GPIO direction (IN / OUT)
GPIO.setup(TRIGGER, GPIO.OUT);
GPIO.setup(ECHO, GPIO.IN);

def rangeFinder():
    # Set trigger to high
    GPIO.output(TRIGGER, True);
    
    # Set trigger after .01s to LOW
    time.sleep(0.00001);
    GPIO.output(TRIGGER, False)

    startTime = time.time();
    stopTime = time.time();

    # Save start time
    while GPIO.input(ECHO) == 0:
        startTime = time.time();

    # Save time of arrival
    while GPIO.input(ECHO) == 1:
        stopTime = time.time();

    # Time different between start and arrival
    timeElapsed = stopTime - startTime;

    # distance formula: distance = time * supersonic speed (34300 cm/s)
    # Since it will travel 2 times (current -> destination, destination -> current) => divided by 2
    distance = (timeElapsed * 34300) / 2;
    return distance

try:
    while True:
        distance = rangeFinder()
        print("Measured distance = %.1f cm" % distance)
        time.sleep(1)

except KeyboardInterrupt:
    print("Measured distance stopped by user")
    GPIO.cleanup()

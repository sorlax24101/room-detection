import RPi.GPIO as GPIO
import time
import math
import pdb

# TRIGGER PORTS
TRIGGER_1 = 27;
TRIGGER_2 = 17;

# ECHO PORTS
ECHO_1 = 4;
ECHO_2 = 18;

# step detection variables
IN_SOUNDWAVE_1 = False;
IN_SOUNDWAVE_2 = False;
OUT_SOUNDWAVE_1 = False;
OUT_SOUNDWAVE_2 = False;
EQUAL_SOUNDWAVE = False;

people_in = 0;
people_out = 0;
people_total = 0;

distance_1 = 0.0;
distance_2 = 0.0;

average_distance_1 = 0.0;
average_distance_2 = 0.0;

distance_lowerbound_1 = 0.0;
distance_lowerbound_2 = 0.0;

distance_upperbound_1 = 0.0;
distance_upperbound_2 = 0.0;

previous_distance_1 = 0.0;
previous_distance_2 = 0.0;

IN_START = False;
OUT_START = False;

in_times = 0.0;
out_times = 0.0;
total_times = 0.0;

MCC = 100;
CNT = 0;

# Settjng up GPIO ports
def setup():
    global TRIGGER_1
    global TRIGGER_2
    global ECHO_1
    global ECHO_2

    GPIO.setmode(GPIO.BCM);

    # us TRIGGER
    GPIO.setup(TRIGGER_1, GPIO.OUT);
    GPIO.setup(TRIGGER_2, GPIO.OUT);
    
    # US ECHO
    GPIO.setup(ECHO_1, GPIO.IN);
    GPIO.setup(ECHO_2, GPIO.IN);
    pass

# counting people going in
def inCount(distance1, distance2):
    global OUT_START
    global IN_START
    global IN_SOUNDWAVE_1
    global IN_SOUNDWAVE_2
    global EQUAL_SOUNDWAVE
    global distance_lowerbound_1
    global distance_upperbound_1
    global distance_lowerbound_2
    global distance_upperbound_2
    global in_times
    global people_in

    #print("Out start: ", OUT_START);
    #print("First in soundwave: ", IN_SOUNDWAVE_1);
    #print("Distance 1: %f\nLower bound: %f" % (distance1, distance_lowerbound_1));
    #print("Distance upper bound 1: ", distance_upperbound_1);
    #print("lower bound comparision: ", (distance_lowerbound_1 > distance1));
    #print("Second distance: %f\tLower bound: %f" % (distance2, distance_lowerbound_2));
    #print("Upper bound: ", distance_upperbound_2);

    if not OUT_START and not IN_SOUNDWAVE_1 and\
            (distance1 < distance_lowerbound_1 or distance1 > distance_upperbound_1) and\
            (distance2 > distance_lowerbound_2 and distance2 < distance_upperbound_2):
        IN_SOUNDWAVE_1 = True;
        IN_START = True;
        #print("Sound wave in 1: ", IN_SOUNDWAVE_1);
        #print("In start: ", IN_START);

    if not IN_SOUNDWAVE_2 and IN_SOUNDWAVE_1 and\
            (distance2 < distance_lowerbound_2 or distance2 > distance_upperbound_2):
                IN_SOUNDWAVE_2 = True;
                #print("Sound wave in 2: ", IN_SOUNDWAVE_2);

    if not OUT_START and IN_SOUNDWAVE_1 and IN_SOUNDWAVE_2 and\
            distance1 > distance_lowerbound_1 and distance1 < distance_upperbound_1 and\
            distance2 > distance_lowerbound_2 and distance2 < distance_upperbound_2:
                in_times += 1;
    else:
        in_times = 0;

    if in_times == 3:
        people_in += 1;
        #print("People going in: ", people_in);
        IN_SOUNDWAVE_1 = False;
        IN_SOUNDWAVE_2 = False;
        IN_START = False;
        EQUAL_SOUNDWAVE = False;

def outCount(distance1, distance2):
    global IN_START
    global OUT_START
    global OUT_SOUNDWAVE_1
    global OUT_SOUNDWAVE_2
    global EQUAL_SOUNDWAVE
    global distance_lowerbound_1
    global distance_lowerbound_2
    global distance_upperbound_1
    global distance_upperbound_2
    global out_times
    global people_out

    if not IN_START and not OUT_SOUNDWAVE_1 and\
            (distance2 < distance_lowerbound_2 or distance2 > distance_upperbound_2) and\
            (distance1 > distance_lowerbound_1 and distance1 < distance_upperbound_1):
                OUT_SOUNDWAVE_1 = True;
                OUT_START = True;

    if not OUT_SOUNDWAVE_2 and OUT_SOUNDWAVE_1 and\
            (distance1 < distance_lowerbound_1 or distance1 > distance_upperbound_1):
                OUT_SOUNDWAVE_2 = True

    if not IN_START and OUT_SOUNDWAVE_1 and OUT_SOUNDWAVE_2 and\
            distance1 > distance_lowerbound_1 and distance1 < distance_upperbound_1 and\
            distance2 > distance_lowerbound_2 and distance2 < distance_upperbound_2:
                out_times += 1;
    else:
        out_times = 0;

    if out_times == 3:
        people_out += 1;
        OUT_SOUNDWAVE_1 = False;
        OUT_SOUNDWAVE_2 = False;
        OUT_START = False;
        EQUAL_SOUNDWAVE = False;

def equalCount(distance1, distance2, p_distance1, p_distance2):
    global distance_lowerbound_1
    global distance_lowerbound_2
    global distance_upperbound_1
    global distance_upperbound_2
    global in_times
    global out_times
    global EQUAL_SOUNDWAVE
    
    if p_distance1 < distance_upperbound_1 and p_distance1 > distance_lowerbound_1 and\
            p_distance2 < distance_upperbound_2 and p_distance2 > distance_lowerbound_2 and\
            (distance1 < distance_lowerbound_1 or distance1 > distance_upperbound_1) and\
            (distance2 < distance_lowerbound_2 or distance2 > distance_upperbound_2):
                EQUAL_SOUNDWAVE = True;

    if EQUAL_SOUNDWAVE is True:
        if distance1 > distance_lowerbound_1 and distance1 < distance_upperbound_1 and\
                distance2 > distance_lowerbound_2 and distance2 < distance_upperbound_2:
                    EQUAL_SOUNDWAVE = False;
        elif distance1 > distance_lowerbound_1 and distance1 < distance_upperbound_1:
            out_times += 1;
        elif distance2 > distance_lowerbound_2 and distance2 < distance_upperbound_2:
            in_times += 1;

def distance1():
    GPIO.output(TRIGGER_1, True);
    time.sleep(0.00001);
    GPIO.output(TRIGGER_1, False);
    
    starttime = time.time();
    stoptime = time.time();

    while GPIO.input(ECHO_1) == 0:
        starttime = time.time();
    while GPIO.input(ECHO_1) == 1:
        stoptime = time.time();

    elapsetime = (stoptime - starttime);

    return ((elapsetime * 34300) / 2)

def distance2():
    GPIO.output(TRIGGER_2, True);
    time.sleep(0.00001);
    GPIO.output(TRIGGER_2, False);

    starttime = time.time();
    stoptime = time.time();

    while GPIO.input(ECHO_2) == 0:
        starttime = time.time();
    while GPIO.input(ECHO_2) == 1:
        stoptime = time.time();

    elapsetime = (stoptime - starttime);

    return ((elapsetime * 34300) / 2)

setup();

arr1 = [0.0] * 100;
arr2 = [0.0] * 100;

try:
    # Loop
    while True:
        previous_distance_1 = distance_1;
        previous_distance_2 = distance_2;
        distance_1 = distance1();
        distance_2 = distance2();

        if CNT < MCC and distance_1 < 400 and distance_2 < 400:
            arr1[CNT] = distance_1;
            arr2[CNT] = distance_2;
        elif CNT == MCC:
            sumResult1 = 0.0
            sumResult2 = 0.0
            for index in range(MCC):
                sumResult1 += pow(arr1[index] - average_distance_1, 2);
                sumResult2 += pow(arr2[index] - average_distance_2, 2);
            square_distance_1 = math.sqrt(sumResult1 / MCC) * 6;
            square_distance_2 = math.sqrt(sumResult2 / MCC) * 6;
            square_distance = 0.0;

            if(square_distance_1 > square_distance_2):
                square_distance = square_distance_1;
            else:
                square_distance = square_distance_2;

            distance_lowerbound_1 = average_distance_1 - square_distance;
            distance_upperbound_1 = average_distance_1 + square_distance;
            distance_lowerbound_2 = average_distance_2 - square_distance;
            distance_upperbound_2 = average_distance_2 + square_distance;
        else:
            inCount(distance_1, distance_2);
            outCount(distance_1, distance_2);
            equalCount(distance_1, distance_2, previous_distance_1, previous_distance_2);
            people_total = people_out - people_in;

        if distance_1 < distance_upperbound_1 and distance_1 > distance_lowerbound_1 and\
                distance_2 < distance_upperbound_2 and distance_2 > distance_lowerbound_2:
                    total_times += 1;
        elif total_times == 3:
            total_times = 0;
            IN_SOUNDWAVE_1 = False;
            OUT_SOUNDWAVE_1 = False;
            IN_SOUNDWAVE_2 = False;
            OUT_SOUNDWAVE_2 = False;
            IN_START = False;
            OUT_START = False;
        else:
            total_times = 0;

        CNT += 1;

        if distance_1 > 100:
            distance_1 = 0;
        if distance_2 > 100:
            distance_2 = 0;

        print("Distance 1: %f" % distance_1);
        print("Distance 2: %f" % distance_2);
        print("Average distance 1: %f" % average_distance_1);
        print("Average distance 2: %f" % average_distance_2);
        print("Distance 1 lower bound: %f" % distance_lowerbound_1);
        print("Distance 1 upperbound: %f" % distance_upperbound_1);
        print("Distance 2 lower bound: %f" % distance_lowerbound_2);
        print("Distance 2 upper bound: %f" % distance_upperbound_2);
        print("People in: %d" % people_in)
        print("People out: %d" % people_out)
        print("Total people: %d" % people_total)

except KeyboardInterrupt:
    print("Program stopped by user")
print("No Errors ^_^ You're good to go")

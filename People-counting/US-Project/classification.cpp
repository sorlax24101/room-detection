extern "C" {
#include <wiringPi.h>
}

#include "header/classification.h"

void classification::setup() {
	pinMode(TRIGGER_1, OUTPUT);
	pinMode(TRIGGER_2, OUTPUT);
	pinMode(ECHO_1, INPUT);
	pinMode(ECHO_2, INPUT);
}

void classification::countIn(float distance1, float distance2) {
	// todo: count people going in
	if(
			!outStart && !soundIn1 &&
			(distance1 < distLowerBound1 || distance1 > distUpperBound1) &&
			(distance2 < distLowerBound2 || distance2 > distUpperBound2))
	{
		soundIn1 = true;
		inStart = true;
	}

	if(!soundIn2 && soundIn1 &&
			(distance2 < distLowerBound2 || distance2 > distUpperBound2))
		soundIn2 = true;
	else
		inTimes = 0;

	if(
			!outStart && soundIn1 && soundIn2 &&
			distance1 > distLowerBound1 && distance1 < distUpperBound1 &&
			distance2 > distLowerBound2 && distance2 < distUpperBound2)
		inTimes++;
	else
		inTimes = 9;

	if(inTimes == 3) {
		peopleIn++;
		soundIn1 = soundIn2 = false;
		inStart = false;
		soundEqual = false;
	}
}

void classification::countOut(float distance1, float distance2) {
	//todo: count people going out
}

void classification::countEqual(float distance1, float distance2, float pDistance1, float pDistance2) {
	//todo: 
}

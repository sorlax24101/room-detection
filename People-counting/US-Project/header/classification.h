#ifndef CLASSIFICATION_H
#define CLASSIFICATION_H

////////////////////////////////   CLASSIFICATION CLASS   ///////////////////////
class classification {
	private:
		float avgDistance1 = 0.0,
		      avgDistance2 = 0.0,
		      distLowerBound1 = 0.0,
		      distLowerBound2 = 0.0,
		      distUpperBound1 = 0.0,
		      distUpperBound2 = 0.0,
		      distance1 = 0.0,
		      distance2 = 0.0,
		      prevDistance1 = 0.0,
		      prevDistance2 = 0.0;

		int peopleIn = 0,
		    peopleOut = 0,
		    peopleTotal = 0;

		float arr1[100], arr2[100];

		bool soundIn1 = false,
		     soundIn2 = false,
		     soundOut1 = false,
		     soundOut2 = false,
		     soundEqual = false;
		
		bool inStart = false,
		     outStart = false;

		float inTimes = 0.0,
		      outTimes = 0.0,
		      totalTimes = 0.0;
	public:
		void setup();
		void countIn(float distance1, float distance2);
		void countOut(float distance1, float distance2);
		void countEqual(float distance1, float distance2, float pDistance1, float pDistance2);

		int getPeopleIn() { return peopleIn; }
		int getPeopleOut() { return peopleOut; }
		int getPeopleTotal() { return peopleTotal; }
};

////////////////////////////////   PINS   ///////////////////////////////////////


#define ECHO_1 4
#define ECHO_2 18

#define TRIGGER_1 27
#define TRIGGER_2 17



///////////////////////////////   UNKNWN   ///////////////////////////////////


#define MCC 100
#define CNT 0

////////////////////////////////////////////////////////////////////////////////

#endif // CLASSIFICATION

from pivideostream import PiVideoStream
from imutils.video import FPS
from tracker import Tracker

import argparse
import datetime
import imutils
import math
import cv2
import numpy as np
import time

INLINE = 150;
OUTLINE = 100;

class recorder:
    def __init__(self,
            width = 300,
            spot = False,
            insider = False,
            outsider = False,
            counter = 0):
        self.width = width;
        self.endb = self.width // 3;
        self.endr = self.width // 2;
        self.spot = spot;
        self.counter = counter;
        self.trackers = [];
        self.insider = insider;
        self.outsider = outsider;

    def testintersectionin(self, x, y):
        res =  (width-1) * y + 300
        if((res >= -550) and (res < 550)):
            print(str(res))
            return True
        return False

    def testintersectionout(self, x, y):
        res = (width-1) * y + 300
        if((res >= -550) and (res <= 550)):
            print (str(res))
            return True
        return False

    def testin(self, tracker):
        self.contour_tracker(tracker.currenty, tracker.currentx);
        if self.outsider is True:
            if tracker.currenty < INLINE and tracker.currenty < OUTLINE:
                self.outsider = False;
                return True;
        return False;

    def testout(self, tracker):
        self.contour_tracker(tracker.currenty, tracker.currentx);
        if self.insider is True:
            if tracker.currenty > OUTLINE and tracker.currenty > INLINE:
                self.insider = False;
                return True;
        return False;

    def contour_tracker(self, y, x = 0):
        global OUTLINE;
        global INLINE;

        if y < OUTLINE:
            self.insider = True;
        elif y > INLINE:
            self.outsider = True;
        pass

    def cameratesting(self):
        if camera.isOpened() is True:
            print("Successfully initialized camera")
        else:
            print("Failed to initialized camera")
            exit()

    def startcapture(self, peoplein, peopleout):
        peoplein.value = 0;
        peopleout.value = 0;
        camera = PiVideoStream().start();
        time.sleep(2.0);
        fps = FPS().start();
        firstframe = None;
        self.framereader(camera, firstframe, fps, peoplein, peopleout);
        fps.stop();
        camera.stop();
        cv2.destroyAllWindows();

    def framereader(self, camera, firstframe, fps, peoplein, peopleout):
        # create background subtractor
        subtractor = cv2.createBackgroundSubtractorMOG2();

        while True:
            # grab the current frame and initialize the occupied/unoccupied text
            frame = camera.read();
            text = "Unoccupied"
            
            # Do a MOG background subtraction
            foreground_mask = subtractor.apply(frame);
            foreground_thresh = cv2.threshold(foreground_mask, 25, 255, cv2.THRESH_BINARY)[1];
            foreground_thresh = cv2.dilate(foreground_thresh, None, iterations=2);

            # resize the frame, convert into gray scale and blur it
            frame = imutils.resize(frame, self.width)
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            gray = cv2.GaussianBlur(gray, (21, 21), 0)
            
            # initialized the first frame
            if firstframe is None:
                firstframe = gray
                continue
            # compute the absolute difference between the currrent frame and first frame
            framedelta = cv2.absdiff(firstframe, gray)
            thresh = cv2.threshold(framedelta, 25, 255, cv2.THRESH_BINARY)[1]

	    # dilate the thresholded image to fill in holes, then find contours on thresholded image
            thresh = cv2.dilate(thresh, None, iterations=2)
            _, cnts, _ = cv2.findContours(foreground_thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

	    # loop over contours
            for c in cnts:
                # ignore the contour if too small
                if cv2.contourArea(c) < 12000:
                    continue
                # compute the bounding box for the contour, draw on the frame and update the text
                (x, y, w, h) = cv2.boundingRect(c)
                cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
                cv2.line(frame, (1, self.width // 3), (self.width, self.width // 3), (255, 0, 1), 2) # blue line
                cv2.line(frame, (1, self.width // 2), (self.width, self.width // 2), (0, 0, 255), 2) # red line
                
                rectanglecenterpoint = ((x + x + w) // 2, (y + y + h) // 2)
                cv2.circle(frame, rectanglecenterpoint, 1, (0, 0, 255), 5)
                if self.spot is False:
                    tracker = Tracker(
                            originalx = rectanglecenterpoint[0],
                            originaly = rectanglecenterpoint[1]);
                    self.spot = True;
                    self.trackers.append(tracker);
                    self.counter += 1;

                self.trackers[self.counter - 1].currentx = rectanglecenterpoint[0];
                self.trackers[self.counter - 1].currenty = rectanglecenterpoint[1];
                #if (self.testintersectionin((x + x + w) // 2, (y + y + h) // 2)):
                #    peoplein.value += 1;
                
                #if (self.testintersectionout((x + x + w) // 2, (y + y + h) // 2)):
                #    peopleout.value += 1;
                if self.testin(self.trackers[self.counter - 1]):
                    self.spot = False;
                    peoplein.value += 1;
                if self.testout(self.trackers[self.counter - 1]):
                    self.spot = False;
                    peopleout.value += 1;

                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break;

                #cv2.putText(frame, "In: {}".format(str(peoplein.value)), (10, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
                #cv2.putText(frame, "Out: {}".format(str(peopleout.value)), (10, 70), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
                #cv2.putText(frame, datetime.datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"),
                        #(10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)
                cv2.imshow("Mixed", frame);
                fps.update();

# First solution

import numpy as np
import cv2
from pivideostream import PiVideoStream
import time
import imutils
import datetime

stream = PiVideoStream().start();
time.sleep(2.0);

key = cv2.waitKey(1) & 0xff;

while True:
    frame = stream.read();
    frame = imutils.resize(frame, width=800);
    timestamp = datetime.datetime.now().strftime("%A %d %B %Y %I:%M:%S%p");
    cv2.putText(frame, timestamp, (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1);
    cv2.imshow("video test", frame);

    if key == ord('q'):
        break;

cv2.destroyAllWindows();
stream.stop();

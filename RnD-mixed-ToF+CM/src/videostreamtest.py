from pivideostream import PiVideoStream
from time import sleep
from imutils.video import FPS

import argparse
import cv2
import imutils
import datetime

video = PiVideoStream().start();
sleep(2.0);
fps = FPS().start();

while True:#fps._numFrames < 100:
    frame = video.read();
    frame = imutils.resize(frame, width=800);

    timestamp = datetime.datetime.now().strftime("%A %d %B %Y %I:%M:%S%p");
    cv2.putText(frame, timestamp, (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1);
    cv2.imshow("Pi Video Stream test", frame);
    if (cv2.waitKey(1) & 0xff) == ord('q'):
        break;

    fps.update();

fps.stop();

print("[INFO] elapse time: {:.2f}".format(fps.elapsed()));
print("[INFO] approx. FPS: {:.2f}".format(fps.fps()));

import threading
import VL53L0X
import cv2

child = 126;#50;
adult = 126;#40;

class tof():
    counter = 0;

    def __init__(self, maxlength=126, distance=0, counter=0):
        self.maxlength = maxlength;
        self.distance = distance;
        self.counter = counter;
        pass

    def rangefinder(self, threadname):
        tof = VL53L0X.VL53L0X();
        tof.start_ranging(VL53L0X.VL53L0X_BEST_ACCURACY_MODE);

        while True:
            self.distance = tof.get_distance() / 10;
            if self.smallerthanmax(self.distance):
                if self.predictchild(self.distance) is True:
                    #print("Child insight!!!");
                    self.counter += 1;

                elif self.predictadult(self.distance) is True:
                   # print("Adult spotted!!!");
                    self.counter += 1;
            #else:
                #print("unidentified object found!!!");

        if cv2.waitKey(1) & 0xFF == ord('q'):
            threadname.exit();
            tof.stop_ranging();

    def predictchild(self, distance):
        global adult;
        height = self.maxlength - distance;
        if height > adult:
            return True;
        return False;

    def predictadult(self, distance):
        global child;
        global adult;
        height = self.maxlength - distance;
        if height < child or height <= adult:
            return True;
        return False;

    def smallerthanmax(self, distance):
        if distance > self.maxlength:
            return False;
        return True

device = tof();

class ToFthread(threading.Thread):
    def __init__(self, threadID, name, counter):
        threading.Thread.__init__(self)
        self.threadID = threadID;
        self.name = name;
        self.counter = counter;

    def run(self):
        global device;
        device.rangefinder(threadname=self.name);
        pass

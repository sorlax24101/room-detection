from pivideostream import PiVideoStream
from imutils.video import FPS
from time import sleep
from tracker import Tracker

import cv2
import imutils
import numpy as np

outsider = False;
insider = False;
pasin = False;
passout = False;
spot = False;

peoplein = 0;
peopleout = 0;
width = 300;
INLINE = 150;
OUTLINE = 100;

trackers = [];
counter = 0;

# red line (second)
def testin(tracker):
    global outsider;
    #res = -150 * x + width^2 * y;
    #res = -450 * x + 400 * y + 157500
    #print(res);
    contour_tracker(tracker.currenty, tracker.currentx);
    if outsider is True:
        # people going in
        if tracker.currenty < INLINE and tracker.currenty < OUTLINE:
            outsider = False;
            return True;
    return False;
    #if( (res >= -550) and (res < 550) ):
    #    return True;
    #return False;

# blue line (first)
def testout(tracker):
    global insider;
    #res = -200 * x + width^2 * y;
    #res = -450 * x + 400 * y + 18000;
    #print(res)
    contour_tracker(tracker.currenty, tracker.currentx);
    if(insider is True):
        #people going out
        if tracker.currenty > OUTLINE and tracker.currenty > INLINE:
            insider = False;
            return True;
    return False;
    #if( (res <= -46000) ):#and (res < 550) ):
    #    return True;
    #return False;

def contour_tracker(y, x=0):
    global insider;
    global outsider;

    if y < OUTLINE:
        insider = True;
        #print("insider spotted!!!");
    elif y > INLINE:
        outsider = True;
        #print("Outsider spotted!!!");
    pass

camera = PiVideoStream().start();
sleep(2.0);
fps = FPS().start();
subtractor = cv2.createBackgroundSubtractorMOG2();
firstframe = None;

while True:
    # read frame
    frame = camera.read();
    frame = imutils.resize(frame, width);

    # absolute difference
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY);
    gray = cv2.GaussianBlur(gray, (21, 21), 0);

    if firstframe is None:
        firstframe = gray;
        continue

    framedelta = cv2.absdiff(firstframe, gray);
    #thresh = cv2.threshold(framedelta, 25, 255, cv2.THRESH_BINARY)[1];
    #thresh = cv2.dilate(thresh, None, iterations=2);

    # apply background subtraction
    foreground_mask = subtractor.apply(framedelta)#frame);
    foreground_thresh = cv2.threshold(foreground_mask, 25, 255, cv2.THRESH_BINARY)[1];
    foreground_thresh = cv2.dilate(foreground_thresh, None, iterations=2);

    _, contours, hierachy = cv2.findContours(foreground_thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE);
    #print("contours: ", contours);
    #print("Number of contours: ", str(len(contours)));
    for contour in contours:
        if cv2.contourArea(contour) < 6000:
            #print("Contour area: ", cv2.contourArea(c));
            continue;
        (x, y, w, h) = cv2.boundingRect(contour);
        #print("X: ", x);
        #print("Y: ", y);

        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2);
        #cv2.drawContours<(frame, contours, c, (0, 255, 0), -1, 8, hierachy);
        centerpoint = ((x + x + w) // 2, (y + y + h) // 2);
        #contour_tracker(y=rectanglecenterpoint[1]);
        #print("x: ", rectanglecenterpoint[0]);
        #print("y: ", rectanglecenterpoint[1]);
        cv2.circle(frame, centerpoint, 1, (0, 0, 255), 5);
        
        if spot is False: #and (
                #(trackers[counter-1].originalx != trackers[counter].originalx and
                #trackers[counter-1].originaly != trackers[counter].originaly) or
                #counter == 0):
            tracker = Tracker(originalx = centerpoint[0], originaly = centerpoint[1]);
            spot = True;
            trackers.append(tracker);
            counter += 1;

            print("tracker original x: ", tracker.get_originalx());
            print("tracker original y: ", tracker.get_originaly());

        trackers[counter-1].set_currentx(centerpoint[0]);
        trackers[counter-1].set_currenty(centerpoint[1]);

        #print("x: ", centerpoint[0]);
        #print("y: ", centerpoint[1]);
        #print("tracker current x: ", tracker.get_currentx());
        #print("tracker current y: ", tracker.get_currenty());
        #print("Outter tracker original x: ", tracker.get_originalx());
        #print("Outter tracker original y: ", tracker.get_originaly());

        if (testin(trackers[counter-1])):
            spot = False;
            #print("In tracker Counter: ", counter);
            #trackers.pop(counter-1);
            #counter -= 1;
            peoplein += 1;
            #print("End in tracker counter: ", counter);

        if (testout(trackers[counter-1])):
            spot = False;
            #print("Out tracker Counter: ", counter);
            #trackers.pop(counter-1);
            #counter -= 1;
            peopleout += 1;
            #print("End out tracker counter: ", counter);

        cv2.line(frame, (0, width // 3), (width * width, width), (255, 0, 1), 2);#blue_out
        cv2.line(frame, (0, width // 2), (width * width, width), (0, 0, 255), 2);#red_in
        cv2.putText(frame, "In:{}".format(str(peoplein)), (10, 50),
                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2);
        cv2.putText(frame, "Out:{}".format(str(peopleout)), (10, 70),
                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2);

    cv2.imshow('normal video', frame);
    #cv2.imshow('background subtraction', foreground_mask);
    cv2.imshow('background subtraction thresh hold', foreground_thresh);
    #cv2.imshow('absolute difference', framedelta);
    #cv2.imshow('absolute difference thresh hold', thresh);
    if cv2.waitKey(1) & 0xff == ord('q'):
        break;

fps.stop();
camera.stop();
cv2.destroyAllWindows();

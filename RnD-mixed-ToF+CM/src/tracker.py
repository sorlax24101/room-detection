class Tracker():

    def __init__(self, currentx = 0, currenty = 0, originalx = 0, originaly = 0):
        self.currentx = currentx;
        self.currenty = currenty;
        self.originalx = originalx;
        self.originaly = originaly;
        pass

    def get_currentx(self): return self.currentx;
    def get_currenty(self): return self.currenty;
    def get_originalx(self):    return self.originalx;
    def get_originaly(self):    return self.originaly;

    def set_originalx(self, originalx):    self.originalx = originalx;
    def set_originaly(self, originaly):    self.originaly = originaly;
    def set_currentx(self, currentx):   self.currentx = currentx;
    def set_currenty(self, currenty):   self.currenty = currenty;
    pass

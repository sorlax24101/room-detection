from picamera import PiCamera

def startrecording():
    name = "sample.h264";
    print("Recording started");
    camera = PiCamera();
    camera.start_recording(name);
    camera.wait_recording(100);
    camera.stop_preview();
    print("recording finished ! ! !");
    return name;

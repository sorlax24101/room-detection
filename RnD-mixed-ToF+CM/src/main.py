from Vfunctions import recorder
from ToFFunctions import *
from multiprocessing import Process, Value
from time import sleep

peoplein = Value('i');
peopleout = Value('i');
video = recorder();
countin = 0;
countout = 0;

video_process = Process(target=video.startcapture, args=(peoplein, peopleout));
ToF_thread = ToFthread(2, "Time of flight thread", 2);

video_process.start();
ToF_thread.start();
print("Finish initializing all threads ! ! !");

# start coding here
tempin = 0;
tempout = 0;
while True:
    if tempin != peoplein.value and peoplein.value > tempin:
        if device.counter > 0:
            countin += 1;
            device.counter -= device.counter;
            print("People going in: ", countin);
        tempin = peoplein.value;
    elif tempout != peopleout.value and peopleout.value > tempout:
        if device.counter > 0:
            countout += 1;
            device.counter -= device.counter;
            print("People going out ", countout);
        tempout = peopleout.value;
    sleep(10);
    print("ToF counter: ", device.counter);

# end coding

video_process.join();
ToF_thread.join();
print("Finish joining all threads ! ! !");

print("End of line main ! ! !");

from pivideostream import PiVideoStream
from imutils.video import FPS

import argparse
import datetime
import imutils
import math
import cv2
import numpy as np
import threading
import time

class recorder:

    width = 800

    textin = 0
    textout = 0

    def __init__(self, textin=0, textout=0, width=800):
        self.textin = textin;
        self.textout = textout;
        self.width = width;

    def subtractIn(self, number):
        self.textin -= number;

    def subtractOut(self, number):
        self.textout -= number;

    def getIn(self):
        return self.textin;

    def getOut(self):
        return self.textout;

    def testintersectionin(self, x, y):
        res = -450 * x + 400 * y + 157500
        if((res >= -550) and (res < 550)):
            print(str(res))
            return True
        return False

    def testintersectionout(self, x, y):
        res = -450 * x + 400 * y + 180000
        if((res >= -550) and (res <= 550)):
            print (str(res))
            return True
        return False

    def cameratesting(self):
        if camera.isOpened() is True:
            print("Successfully initialized camera")
        else:
            print("Failed to initialized camera")
            exit()

    def startcapture(self):
        print("Capturing");
        #camera = cv2.VideoCapture(name);
        camera = PiVideoStream().start();
        time.sleep(2.0);
        fps = FPS().start();
        firstframe = None;
        print("Analyzing");
        self.framereader(camera, firstframe, fps);
        print("Stopping");
        fps.stop();
        camera.release();
        cv2.destroyAllWindows();

    def framereader(self, camera, firstframe, fps):
        while True:
            # grab the current frame and initialize the occupied/unoccupied text
            #(grabbed, frame) = camera.read()
            frame = camera.read()
            text = "Unoccupied"
            
            # if the frame could not be grabbed, the end of video is reached
            #if not grabbed:
            #    break
            
            # resize the frame, convert into gray scale and blur it
            frame = imutils.resize(frame, self.width)
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            gray = cv2.GaussianBlur(gray, (21, 21), 0)
            
            # initialized the first frame
            if firstframe is None:
                firstframe = gray
                continue
            # compute the absolute difference between the currrent frame and first frame
            framedelta = cv2.absdiff(firstframe, gray)
            thresh = cv2.threshold(framedelta, 25, 255, cv2.THRESH_BINARY)[1]

	    # dilate the thresholded image to fill in holes, then find contours on thresholded image
            thresh = cv2.dilate(thresh, None, iterations=2)
            _, cnts, _ = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

	    # loop over contours
            for c in cnts:
                # ignore the contour if too small
                if cv2.contourArea(c) < 12000:
                    continue
                # compute the bounding box for the contour, draw on the frame and update the text
                (x, y, w, h) = cv2.boundingRect(c)
                cv2.rectangle(frame, (x, y), (x + y, w + h), (0, 255, 0), 2)
                cv2.line(frame, (self.width // self.width, self.width // 3), (self.width * self.width, self.width), (255, 0, 1), 2) # blue line
                cv2.line(frame, (self.width // self.width, self.width // 2), (self.width * self.width, self.width), (0, 0, 255), 2) # red line
                
                rectanglecenterpoint = ((x + x + w) // 2, (y + y + h) // 2)
                cv2.circle(frame, rectanglecenterpoint, 1, (0, 0, 255), 5)
                if (self.testintersectionin((x + x + w) // 2, (y + y + h) // 2)):
                    self.textin += 1
                
                if (self.testintersectionout((x + x + w) // 2, (y + y + h) // 2)):
                    self.textout += 1
                
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
                cv2.putText(frame, "In: {}".format(str(self.textin)), (10, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
                cv2.putText(frame, "Out: {}".format(str(self.textout)), (10, 70), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
                cv2.putText(frame, datetime.datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"),
                        (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)
                cv2.imshow("Mixed", frame)
                fps.update();

#video = recorder();

#class videoThread(threading.Thread):
#    def __init__(self, threadID, name, counter):
#        threading.Thread.__init__(self);
#        self.threadID = threadID;
#        self.name = name;
#        self.counter = counter;
    
#    def run(self):
#        global video
#        video.startcapture(threadname=self.name);
#        pass

import threading
import time

exitFlag = False

def print_time(threadName, delay, counter):
    while counter:
        if exitFlag:
            threadName.exit();
        time.sleep(delay);
        print("%s: %s" % (threadName, time.ctime(time.time())));

class sampleThread(threading.Thread):
    def __init__(self, threadID, name, counter):
        threading.Thread.__init__(self);
        self.threadID = threadID;
        self.name = name;
        self.counter = counter;

    def run(self):
        print("Starting " + self.name);
        print_time(self.name, self.counter, 5);
        print("Exiting " + self.name);

# Creating threads
thread1 = sampleThread(1, "Thread 1", 1);
thread2 = sampleThread(2, "Thread 2", 2);

# Run threads
thread1.start();
thread2.start();
thread1.join();
print("MAIN THREAD")
thread2.join();

print("Exiting Main thread");
